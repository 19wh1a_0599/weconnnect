import React from 'react';
import "./HomepageHeader.css";
import { useNavigate } from "react-router-dom";
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import VideoCallIcon from '@mui/icons-material/VideoCall';
import NotificationsIcon from '@mui/icons-material/Notifications';
import 'bootstrap/dist/css/bootstrap.min.css';
import DuoIcon from '@mui/icons-material/Duo';
import Avatar from '@mui/material/Avatar';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import LogoutIcon from '@mui/icons-material/Logout';
import OndemandVideoIcon from '@mui/icons-material/OndemandVideo';

function HomepageHeader() {
    let navigate = useNavigate();
    const routechange = () => {
        let path = `/login`;
        navigate(path);
    }
    return (
        <div className="header">
            <div className="header_left">
                <MenuIcon />
                <img className="header_logo" src="https://cdn.dribbble.com/users/384646/screenshots/10774919/c-letter-play-button-online-video-streaming-logo_4x.png">
                </img>
            </div>

            <div className="header_input">
                <input placeholder="Search" type="text" />
                <SearchIcon className="header_inputbutton" />
            </div>

            <div className="header_right">
                <div className="dropdown">
                    <VideoCallIcon className="header_right1" />
                    <div className="dropdown-content">
                        <a href="#"><OndemandVideoIcon className="pad" />Upload Video</a>
                        <a href="#"><DuoIcon className="pad" />Create Room</a>
                    </div>
                </div>

                <NotificationsIcon className="header_right2" />
                <div className="dropdown">
                    <Avatar src=" https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRd1tBmwfmXaQ52yEq82N5ca_5_fWqfxd-I7g&usqp=CAU" />
                    <div className="dropdown-content">
                        <a href="#"><AccountCircleIcon className="pad" />Your Channel</a>
                        <a href="#"><LogoutIcon className="pad" />Sign Out</a>
                    </div>
                </div>
                {/* <button className="but" type="button" class="btn btn-primary" onClick={routechange}>SIGN IN</button> */}
            </div>
        </div>


    )
}

export default HomepageHeader

