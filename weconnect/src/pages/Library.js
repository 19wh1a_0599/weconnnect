import React from 'react'
import Sidebar from './sidebar'
import HomepageHeader from './HomepageHeader'
import HomepageSidebar from './HomepageSidebar'

function Library() {
    return (
        <div>
            <HomepageHeader />
            <div className="app_page">
                <HomepageSidebar />
                <div>
                    <h3>Feature coming Soon</h3>
                </div>
            </div>
        </div>
    )
}

export default Library
