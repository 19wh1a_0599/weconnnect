# weConnnect
 
 This is a video streaming Application and is made with MERN stack.On here,You are able to search for videos,watch video ,see the comments for the videos, upload videos.

 weConnect includes a feature of Video chat to watch videos with friends online. It enables both video calls and chat while you're streaming the same video.video chat turn social distancing into distant socializing ·


 goal of this project was to take a wireframe or template and translate that into a functioning and visually web app.


 ## Technologies used

- React (Including react router)
- MongoDB 
- Material UI
- node.js
- Express.js
- Ant Design


 ## Features

- Registration 
- Sign in

- Upload videos
   - thumbnail 
   - title 
   - description
   - visibility
   - category

- Dashboard 
  - Side Bar
       - Home 
       - History
       - Trending
       
   
  - Header
       - Logo (directs to home)
       - Sign in , Sign out
       - Create Video
       - Create room (yet to be integrated)
       - Search Bar

 - Body
       - Recommended videos
       




## Start The Application

npm install to install all dependencies

npm run dev (after cd server) to run server

npm start (after cd weconnect) to run client


## Team Members

- Donuru Nidhi - 19WH1A0507 (Team leader)

- Harini Gannamaneni - 19WH1A0510

- Namburi Sai Sanjana - 19WH1A1263

- Kunjeti Sai Preethi - 19WH1A0215

- Bhupathiraju Devi Poojitha - 19WH1A0599






