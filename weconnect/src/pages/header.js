import React from 'react';
import "./Header.css";
import { useNavigate } from "react-router-dom";
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import VideoCallIcon from '@mui/icons-material/VideoCall';
import NotificationsIcon from '@mui/icons-material/Notifications';
import 'bootstrap/dist/css/bootstrap.min.css';
import Avatar from '@mui/material/Avatar';

function Header() {
    let navigate = useNavigate();
    const routechange = () => {
        let path = `/login`;
        navigate(path);
    }
    return (
        <div className="header">
            <div className="header_left">
                <MenuIcon />
                <img className="header_logo" src="https://cdn.dribbble.com/users/384646/screenshots/10774919/c-letter-play-button-online-video-streaming-logo_4x.png">
                </img>
            </div>

            <div className="header_input">
                <input placeholder="Search" type="text" />
                <SearchIcon className="header_inputbutton" />
            </div>

            <div className="header_right">
                <VideoCallIcon className="header_right1" />
                <NotificationsIcon className="header_right2" />
                {/* <Avatar className="header_right3" src=" https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRd1tBmwfmXaQ52yEq82N5ca_5_fWqfxd-I7g&usqp=CAU" /> */}
                <button className="but" type="button" class="btn btn-primary" onClick={routechange}>SIGN IN</button>
            </div>


        </div>
    )
}

export default Header

