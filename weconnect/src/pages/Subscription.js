import React from 'react'
import Sidebar from './sidebar'
import HomepageHeader from './HomepageHeader'
import HomepageSidebar from './HomepageSidebar'

function Subscription() {
    return (
        <div>
            <HomepageHeader />
            <div className="app_page">
                <HomepageSidebar />
                <div>
                    <p>No subsctiption videos</p>
                </div>
            </div>
        </div>
    )
}

export default Subscription
