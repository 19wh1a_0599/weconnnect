import React from 'react'
import HomepageHeader from './HomepageHeader'
import Sidebar from './sidebar'
import Channelheader from './Channelheader'
import HomepageSidebar from './HomepageSidebar'
import RecommendedVideos from './recommendedvideos'
import Login from './login'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

function Channel() {
    return (
        <div>
            <HomepageHeader />
            <div className="app_page">
                <HomepageSidebar />
                <Channelheader />
            </div>

        </div>


    )
}

export default Channel