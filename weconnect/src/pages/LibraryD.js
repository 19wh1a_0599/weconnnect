import React from 'react'
import Sidebar from './sidebar'
import Header from './header';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useNavigate } from "react-router-dom";
import './LibraryD.css'

function LibraryD() {
    let navigate = useNavigate();
    const routechange = () => {
        let path = `/login`;
        navigate(path);
    }

    return (
        <div>
            <Header />
            <div className="app_page">
                <Sidebar />
                <div className="lib">
                    <h2>Enjoy your favorite videos</h2>
                    <p>Sign in to access videos that you’ve liked or saved
                    </p>
                    <button className="but" type="button" class="btn btn-primary" onClick={routechange}>SIGN IN</button>

                </div>
            </div>
        </div>
    )
}

export default LibraryD
