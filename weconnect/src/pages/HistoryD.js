import React from 'react'
import Sidebar from './sidebar'
import Header from './header';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useNavigate } from "react-router-dom";
import './HistoryD.css'

function HistoryD() {
    let navigate = useNavigate();
    const routechange = () => {
        let path = `/login`;
        navigate(path);
    }

    return (
        <div>
            <Header />
            <div className="app_page">
                <Sidebar />
                <div className="subscribe">
                    <h2>Keep track of what you watch</h2>
                    <p>Watch history isn't viewable when signed out.</p>
                    <button className="but" type="button" class="btn btn-primary" onClick={routechange}>SIGN IN</button>

                </div>
            </div>
        </div>
    )
}

export default HistoryD
