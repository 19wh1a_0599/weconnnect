import React from 'react'
import Header from './header'
import Sidebar from './sidebar'
import RecommendedVideos from './recommendedvideos'
import Login from './login'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

function Dashboard() {
    return (
        <div>
            <Header />
            <div className="app_page">
                <Sidebar />
                <RecommendedVideos />
            </div>
        </div>


    )
}

export default Dashboard