import React from 'react'
import Avatar from '@mui/material/Avatar';
import "./Channelheader.css";

function Channelheader() {
    return (
        <div className="cha">
            <div className="head">
                <div className="head1">
                    <Avatar sx={{ height: '70px', width: '70px' }} className="avatar" src=" https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRd1tBmwfmXaQ52yEq82N5ca_5_fWqfxd-I7g&usqp=CAU" />
                    <div className="user">
                        <h4>poojitha</h4>
                        <p>0 subscribers</p>
                    </div>
                    <div className="sub">
                        <button>SUBSCRIBE</button>
                    </div>
                </div>
            </div>
            <div className="body">
                <h4>Nothing to Show</h4>
            </div>
        </div>
    )
}

export default Channelheader
