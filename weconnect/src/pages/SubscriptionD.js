import React from 'react'
import Sidebar from './sidebar'
import Header from './header';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useNavigate } from "react-router-dom";
import './SubscriptionD.css'

function SubscriptionD() {
    let navigate = useNavigate();
    const routechange = () => {
        let path = `/login`;
        navigate(path);
    }

    return (
        <div>
            <Header />
            <div className="app_page">
                <Sidebar />
                <div className="subscribe">
                    <h2>Don't miss new Videos</h2>
                    <p>Sign in to see updates from your favorite weConnect channels</p>
                    <button className="but" type="button" class="btn btn-primary" onClick={routechange}>SIGN IN</button>

                </div>
            </div>
        </div>
    )
}

export default SubscriptionD
