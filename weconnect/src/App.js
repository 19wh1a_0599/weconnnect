import React from 'react'
import "./App.css";
import Login from './pages/login'
import Register from './pages/register'
import Dashboard from './pages/dashboard'
import Homepage from './pages/HomePage';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import TrendingD from './pages/TrendingD';

const App = () => {
  return (
    <div>
      <Router>
        <Routes>
          {/* <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} /> */}
          <Route path="/" element={<Dashboard />} />
          <Route path='/login' element={<Login />} />
          <Route path='/register' element={<Register />} />
          <Route path='/homepage' element={<Homepage />} />
          <Route path='/TrendingD' element={<TrendingD/>} />
          <Route path='/UploadVid' element={<TrendingD/>} />

        </Routes>
      </Router>
    </div>
  )
}

export default App